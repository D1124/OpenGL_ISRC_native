# OpenGL_ISRC_native

#### 介绍
opengl鸿蒙项目native部分使用示例

#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/161706_c5451a30_8547337.png "屏幕截图.png")
在OpenGL的Java项目的基础上添加了native部分，用来创建提供给ETC1使用的so库，生成库的cpp文件为：etc1_util.cpp。
该库包括Opengl中ETC1、Martix、Visibility三个类的native方法，缺少GLUtils类的方法（因为需要将bitmap转为pixelmap所以没有移植）。

#### 使用说明

1.  在创建鸿蒙项目时选择Native C++项目：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/162951_5df9badf_8547337.png "屏幕截图.png")

2.  在CMakeList.txt文件中添加：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/163145_9ec25f36_8547337.png "屏幕截图.png")

3.  在etc1代码所在的cpp文件中添加：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/163319_6449c292_8547337.png "屏幕截图.png")

红色框中为主要代码，蓝色框中为对应的Java方法路径：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/163514_f004ec6f_8547337.png "屏幕截图.png")

4.最后在需要使用so库的Java文件中添加一段代码来使用so库：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/163647_cc1f3a7a_8547337.png "屏幕截图.png")

5.运行后生成的so文件会生成在build文件中：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/164101_936b06f4_8547337.png "屏幕截图.png")


#### 链接

etc1_util.cpp代码参考链接：http://androidxref.com/9.0.0_r3/xref/frameworks/base/core/jni/android/opengl/util.cpp

项目文件中etc1的代码：http://androidxref.com/9.0.0_r3/xref/frameworks/native/opengl/libs/ETC1/etc1.cpp

可能需要移植的与shader相关的代码：http://androidxref.com/9.0.0_r3/xref/external/swiftshader/

